import { createWebHistory, createRouter } from 'vue-router';

import home from './pages/home.vue';
import login from './pages/login.vue';
import register from './pages/register.vue';
import notFound from './pages/notFound.vue';


import store from './store'

const routes = [
    {
        name: 'Home',
        path: '/',
        component: home,
        meta: {
            requiresAuth: true
        }
    },
    {
        name: 'Login',
        path: '/login',
        component: login,
        meta: {
            requiresAuth: false
        }
    },
    {
        name: 'Register',
        path: '/register',
        component: register,
        meta: {
            requiresAuth: false
        }
    },
    {
        path: '/:pathMatch(.*)*',
        component: notFound
    }

];

const router = createRouter({
    history: createWebHistory(),
    routes
});

router.beforeEach((to, from) => {
    if (to.meta.requiresAuth && store.getters.getToken == 0) {
        return { name: 'Login' }
    }
    if (to.meta.requiresAuth === false && store.getters.getToken) {
        return { name: 'Home' }
    }
});

export default router;