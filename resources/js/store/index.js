import { createStore } from 'vuex';

const store = createStore({

    state: {
        // definne variable
        token: localStorage.getItem('_token') || 0
    },
    mutations: {
        // update varable value
        UPDATE_TOKEN(state, payload) {
            state.token = payload;
        }
    },
    actions: {
        // perfom action
        setToken(context, payload) {
            localStorage.setItem('_token', payload);
            context.commit('UPDATE_TOKEN', payload);
        },
        removeToken(context) {
            localStorage.removeItem('_token');
            context.commit('UPDATE_TOKEN', 0);
        }
    },
    getters: {
        //  get state value
        getToken: function (state) {
            return state.token;
        }
    }

});

export default store;
