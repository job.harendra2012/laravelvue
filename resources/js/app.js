import './bootstrap';

/* Start Vue js  */
import { createApp } from 'vue';

import app from './layouts/app.vue';
import routes from './routes';
import store from './store';

createApp(app)
    .use(routes)
    .use(store)
    .mount("#app")
