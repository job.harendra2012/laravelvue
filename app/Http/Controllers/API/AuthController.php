<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $message = [
            'password.regex' => 'Password must has at least 8 characters and contain at least 1 number and 1 special character (!@#$%^&*_)',
        ];
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'min:2', 'max:100'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'min:8', 'max:40', 'regex:/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&_^])[A-Za-z\d@$!%*#?&_^]{8,}$/'],
            'confirm_password' => ['required', 'required_with:password', 'same:password'],
        ], $message);
        if ($validator->fails()) {
            $error = "Please fill the required field.";
            $errorMessages = $validator->errors();
            return sendError($error, $errorMessages, 422);
        }

        try {
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);

            $success['token'] = $user->createToken('MyApp')->plainTextToken;
            $success['name'] = $user->name;

            $message = "User register successfully";
            $errorMessages = [];
            return sendResponse($success, $message);
        } catch (\Throwable $th) {
            $error = "Invalid request.";
            $errorMessages = [];
            return sendError($error, $errorMessages, 400);
        }
    }

    public function login(Request $request)
    {
        $message = [
            'email.required' => 'Email address is required.',
            'email.email' => 'Email address must be valid.',
            'email.exists' => 'Incorrect email address.',
            'password.required' => 'Password is required.',
        ];
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
        ], $message);
        if ($validator->fails()) {
            $error = "Please fill the required field.";
            $errorMessages = $validator->errors();
            return sendError($error, $errorMessages, 422);
        }

        try {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = Auth::user();
                $success['token'] = $user->createToken('MyApp')->plainTextToken;
                $success['name'] = $user->name;
                $message = "User login successfully";
                $errorMessages = [];
                return sendResponse($success, $message);
            } else {
                $error = "Invalid login credentials.";
                $errorMessages = [];
                return sendError($error, $errorMessages, 200);
            }
        } catch (\Throwable $th) {
            $error = "Invalid request.";
            $errorMessages = [];
            return sendError($error, $errorMessages, 400);
        }
    }
}
